'use strict';

const fetch = require('./fetch.js');

const call = (method, data) => {
    const id = Math.random().toString(36).slice(2);
    console.log(`CALL[${id}]: ${method}\n`, data);
    return fetch(`https://api.telegram.org/bot${process.env.BOT_TOKEN}/${method}`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    }).then((res) => {
        console.log(`RES[${id}]:`, res.status);
        return res.json();
    }).then((data) => {
        console.log(`RES[${id}]:`, data);
        return data;
    }).catch((error) => {
        console.log(`FAIL[${id}]`);
        throw error;
    });
};

const mute = (chat_id, user_id, until_date) => call('restrictChatMember', {
    chat_id,
    user_id,
    until_date,
    permissions: {
        can_send_messages: false,
        can_send_media_messages: false,
        can_send_polls: false,
        can_send_other_messages: false,
        can_add_web_page_previews: false,
        can_change_info: false,
        can_invite_users: false,
        can_pin_messages: false
    }
});

const deleteMessage = (chat_id, message_id) => call('deleteMessage', { chat_id, message_id });

const userInfo = (chat_id, user_id) => call('getChatMember', { chat_id, user_id });

const isTgUrl = ({ type, offset }, text) => type === 'url' && text.slice(offset, offset + 5) === 't.me/';

exports.handler = async (event, context) => {
    const now = new Date();
    let data;
    try {
        data = JSON.parse(event.body);
    } catch (error) {
        console.log(error);
        return {
            statusCode: 400,
            body: 'Bad request'
        }
    }

    if (!data.update_id || !data.message) {
        console.log('Not a message', data);
        return {
            statusCode: 200,
            body: 'Ignored'
        };
    }

    data = data.message;
    const message_time = new Date(data.date * 1000);
    console.log('bot:', message_time);
    if (now - message_time > 60000) {
        console.log('Message too old. Ignored', data);
        return {
            statusCode: 200,
            body: 'Ignored stale message'
        };
    }
    console.log('Message:', data);

    if (data.new_chat_members) {
        const chat_id = data.chat.id;
        const until_date = data.date + 300; // 5 minutes
        const p = data.new_chat_members.map(({ id }) => mute(chat_id, id, until_date));
        p.push(deleteMessage(chat_id, data.message_id));
        try {
            await Promise.all(p);
        } catch (error) {
            console.log(error);
            return {
                statusCode: 500,
                body: 'Bad server'
            }
        }
    }

    if (data.reply_to_message) {
        if (data.entities && data.entities.some(ent => isTgUrl(ent, data.text))) {
            const { result } = await userInfo(data.chat.id, data.from.id);
            if (result.status === 'left') {
                await deleteMessage(data.chat.id, data.message_id);
            }
        }
    }

    return {
        statusCode: 200,
        body: 'ok'
    };
};
